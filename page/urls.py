from django.urls import include,path
from .views import index, about, aktivitassaya, fungsi_tampilkan_form, hasilform, del_jadwal,books, search_books

urlpatterns = [
    path('', index, name='index'),
    path('about/', about, name='about'),
    path('aktivitassaya/', aktivitassaya, name='aktivitassaya'),
    path('form/', fungsi_tampilkan_form, name="formulir"),
    path('hasil_form/', hasilform, name="hasilform"),
    path('deletejadwal/<int:identitas>/', del_jadwal, name="hapusjadwal"),
    path('cari_buku/', books, name="books"),
    path('cari_buku/<title>', search_books, name="search_books"),
]
