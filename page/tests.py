from django.test import TestCase
from .views import *

class BookTest(TestCase) :
    def test_url_cari_buku_exists(self):
        response = self.client.get('/cari_buku/')
        self.assertEqual(response.status_code, 200)

    def test_cari_buku_template(self):
        response = self.client.get('/cari_buku/')
        self.assertTemplateUsed("page/ajax.html")

    def test_index_template(self):
        response = self.client.get('')
        self.assertTemplateUsed("index.html")


    def test_search_book_forms(self):
        response = self.client.get(reverse('search_books', args=['Lord of the Rings']))
        self.assertIn("The Fellowship of the Ring", response.content.decode())

# Create your tests here.
