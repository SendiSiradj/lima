from django import forms
from django.forms import ModelForm
import datetime

class ActivityForm(forms.Form):
    your_activity = forms.CharField(label='Nama Kegiatan', max_length=100)
    date = forms.DateTimeField(initial=datetime.date.today)
    place = forms.CharField(label="Tempat", max_length=20)
    category = forms.CharField(label="Kategori", max_length=20)
