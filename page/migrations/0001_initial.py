# Generated by Django 2.2.5 on 2019-10-08 11:45

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('activity', models.CharField(max_length=50)),
                ('date', models.DateField()),
                ('place', models.CharField(max_length=50)),
                ('kategori', models.CharField(max_length=20)),
            ],
        ),
    ]
