from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import Activity
from .forms import ActivityForm
from django.http import JsonResponse
import requests
import json
from .forms import ActivityForm



def index(request) :
    return render (request, 'index.html')


def aktivitassaya(request) :
    return render (request, 'aktivitassaya.html')

def about(request) :
    return render (request, 'about.html')


def fungsi_tampilkan_form(request) :
    form = ActivityForm(request.POST)
    response = {'form' : ActivityForm()}
    if(request.method == 'POST' and form.is_valid()):
        # response['activity'] = request.POST['activity']
        # response['date'] = request.POST['date']
        # response['place'] = request.POST['place']
        # response['kategori'] = request.POST['kategori']
        activity = form.cleaned_data['your_activity']
        date = form.cleaned_data['date']
        place = form.cleaned_data['place']
        kategori = form.cleaned_data['category']
        p = Activity(activity =activity, date=date, place=place, kategori=kategori)
        p.save()
        return HttpResponseRedirect(reverse("hasilform"))
    else:
        return render(request, 'form.html', response)


def hasilform(request) :

    jadwal = Activity.objects.all()
    response = {'tabel' : jadwal}
    return render(request, 'hasil_form.html', response)

def del_jadwal(request, identitas) :
    Activity.objects.get(id=identitas).delete()
    return HttpResponseRedirect(reverse("hasilform"))


def books(request) :
    return render(request, 'ajax.html')

def search_books(request, title) :
    response = requests.get('https://www.googleapis.com/books/v1/volumes?q=intitle:' + title)
    return JsonResponse(response.json())
