from django.db import models
from django.forms import ModelForm

class Activity(models.Model) :
        activity = models.CharField(max_length=50)
        date = models.DateField()
        place = models.CharField(max_length=50)
        kategori = models.CharField(max_length=20)

        def __str__(self):
            return self.activity
