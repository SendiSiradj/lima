$(document).ready(function(){
  bookSearch("dictionary");
});

function readInput(){
  var title = $('#input').val();
  $('#input').val("");
  bookSearch(title);
}


function bookSearch(title){
  // var search = document.getElementById('input').value
  // document.getElementById('results').innerHTML = ""
  // console.log(search)
  $("tbody").empty();
  $.ajax({
    url : title,
    success: function(data) {
      // for(i = 0; i < data.items.length; i++){
      //   results.innerHTML += "<h2>" + data.items[i].volumeInfo.title + "</h2>"
      // }
      if(data.totalItems != 0) {
        data.items.forEach(function(book, index){
          $("tbody").append(`
            <tr>
              <td>${index + 1}</td>
              <td><img src="${book.volumeInfo.imageLink.smallThumbnail}"></td>
              <td>${book.volumeInfo.title}</td>
              <td>${book.volumeInfo.authors}</td>
              <td>${book.volumeInfo.publishers}</td>
              <td>${book.volumeInfo.pageCount}</td>
            </tr>
            `);
        });
      }  else {
        $("tbody").append(`
          <tr>
            <td colspan=6 class="text-center">No search results found</td>
          </tr>
          `);
      }
    }
  });
}
