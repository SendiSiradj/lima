$(document).ready(function(){
  bookSearch("dictionary");
});

function readInput(){
  var title = $('#input').val();
  $('#input').val("");
  bookSearch(title);
}


function bookSearch(title){
  // var search = document.getElementById('input').value
  // document.getElementById('results').innerHTML = ""
  // console.log(search)
  $("#booklist").empty();
  $.ajax({
    url :  title,
    success: function(data) {

      if(data.totalItems != 0) {
        data.items.forEach(function(book, index) {
            $("#booklist").append(`
                <tr>
                  <td>${index + 1}</td>
                  <td><img src="${book.volumeInfo.imageLinks.thumbnail}"</td>
                  <td>${book.volumeInfo.title}</td>
                  <td>${book.volumeInfo.authors}</td>
                  <td>${book.volumeInfo.publisher}</td>
                  <td>${book.volumeInfo.pageCount}</td>
                </tr>
                  `);

                } );
    } else {
        $("#booklist").append(`
          <tr>
            <td colspan=6 class="text-center">No search results found</td>
          </tr>
          `);
      }
    }
  });
}
